import json

from room import Room
from room_interface.room_parser.room_parser import RoomParser
from effects.effect_creator import EffectCreator
from effects.effect import Effect


class JsonRoomParser(RoomParser):
    def __init__(self, effect_creator: EffectCreator):
        self.effect_creator = effect_creator

    def parse_actions(self, actions_data: dict) -> 'dict[str, Effect]':
        """
        Creates actions dict [str, list[Effect]] from actions dict [str, list[dict[str, str]]]
        :param actions_data: data from json: dict [str, list[Effect]]
        :return: actions dict [str, list[Effect]]
        """
        actions = {}
        for name in actions_data:
            lst = [self.effect_creator.create(effect) for effect in actions_data[name]]
            actions.update({name: lst})
        return actions

    def parse(self, json_text: str) -> Room:
        """
        Creates Room from json string
        :str json_text: json text representation of room
        """
        data = json.loads(json_text)
        is_exit_room = data['is_exit_room'] if 'is_exit_room' in data else False
        acts = self.parse_actions(data['actions'])
        return Room(int(data["id"]), data["name"], data["description"], acts, is_exit_room)
