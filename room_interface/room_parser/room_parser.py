from room import Room


class RoomParser:
    """Room parser interface. Don't use directly. Parse text format to Room object"""

    def parse(self, text_format: str) -> Room:
        """
        Creates Room from text format
        :str text_format: object's text representation
        """
        pass
