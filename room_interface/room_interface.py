from room import Room


class RoomInterface:
    """
    The room interface to external source
    """
    def get_room_by_id(self, room_id: int) -> Room:
        """
        reads Room object from external source
        :int room_id: unique room id
        """
        pass
