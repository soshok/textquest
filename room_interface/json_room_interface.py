from room_interface.room_interface import RoomInterface
from room_interface.room_parser.json_room_parser import JsonRoomParser
from effects.effect_creator import EffectCreator
from room import Room


class JsonRoomInterface(RoomInterface):
    """
    Json implementation for RoomInterface
    """
    def __init__(self, game_abs_path: str, effect_creator: EffectCreator):
        """
        :str game_abs_path: absolute path to directory with game
        """
        self.room_parser = JsonRoomParser(effect_creator)
        self.game_abs_path = game_abs_path

    def get_room_by_id(self, room_id: int) -> Room:
        """
        read room from json file by id
        """
        file = open(f'{self.game_abs_path}/{room_id}.json', 'r', encoding='utf8')
        return self.room_parser.parse(file.read())
