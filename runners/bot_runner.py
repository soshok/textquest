import telebot
from telebot import types

from engine import Engine
from room_interface.json_room_interface import JsonRoomInterface
from effects.effect_creator import EffectCreator


class TextQuestBot:
    def __init__(self, bot: telebot.TeleBot, game_data_folder: str, engines):
        self.bot = bot
        self.game = engines
        self.game_data_folder = game_data_folder

    def create_new_engine(self):
        engine = Engine()
        room_interface = JsonRoomInterface(self.game_data_folder, EffectCreator(engine))
        engine.set_room_interface(room_interface)
        engine.go_to_room(1)
        return engine

    def start(self):
        @self.bot.message_handler(commands=['again'])
        def again_message(message):
            self.game[message.chat.id] = self.create_new_engine()
            msg(message)

        @self.bot.message_handler(commands=['start'])
        def start_message(message):
            if message.chat.id in self.game:
                self.bot.send_message(message.chat.id, 'Игра уже во всю идёт. Давай погнали!')
            else:
                self.bot.send_message(message.chat.id, 'Добро пожаловать в самую захватывающую игру мире!')
                self.game[message.chat.id] = self.create_new_engine()
            msg(message)

        @self.bot.message_handler()
        def msg(message):
            if message.chat.id not in self.game:
                self.bot.send_message(message.chat.id, 'Запускай! /start')
                return

            game = self.game[message.chat.id]

            try:
                game.take_action(message.text)
            except RuntimeError as ex:
                print(str(ex))
            markup = types.ReplyKeyboardMarkup()

            for i in game.available_actions():
                markup.row(i)
            self.bot.send_message(message.chat.id, game.current_room.description, reply_markup=markup)

            if game.current_room.is_exit_room:
                self.bot.send_message(message.chat.id, 'Начать заново /again', reply_markup=None)


if __name__ == '__main__':
    tb = telebot.TeleBot(input("Введите токен для telegram бота:"))
    quest = TextQuestBot(tb, 'D:/py/textquest/data', {})
    quest.start()
    tb.polling()
