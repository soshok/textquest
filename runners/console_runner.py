from effects.effect_creator import EffectCreator
from engine import Engine
from room_interface.json_room_interface import JsonRoomInterface


def print_error(err: str):
    print('----------------------------------------------------------')
    print(err)
    print('----------------------------------------------------------')


if __name__ == '__main__':
    room_interface = None
    game = Engine()
    try:
        room_interface = JsonRoomInterface('D:/py/textquest/data', EffectCreator(game))
        game.set_room_interface(room_interface)
        game.go_to_room(1)
    except FileNotFoundError as ex:
        print(ex)
        exit()

    while not game.current_room.is_exit_room:
        print(game.current_room)
        print()
        try:
            game.take_action(input('next room:'))
        except RuntimeError as ex:
            print(ex)
        except FileNotFoundError as ex:
            print_error(f'Ошибка в шаблоне игры: {ex}')

    print(game.current_room)
