class Effect:
    """
    Base effect interface. Inherited classes define behavior related to actions. Don't use directly
    """
    def exec(self):
        """
        Triggers a reaction to related action
        :return: None
        """
        pass
