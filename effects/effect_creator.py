from engine import Engine
from effects.effect import Effect
from effects.move_effect import MoveEffect


class EffectCreator:
    def __init__(self, engine: Engine):
        """
        :param engine: main object in the game
        """
        self.engine = engine

    def create(self, effect_data: dict) -> Effect:
        """
        creates effect according to effect type
        :param effect_data: dictionary with action type and additional data
        :return: Effect object
        """
        if effect_data['type'] == 'move':
            return MoveEffect(self.engine, effect_data['next_room'])

        raise NotImplementedError(f'there is no type: {effect_data["type"]}')
