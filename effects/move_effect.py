from effects.effect import Effect
from engine import Engine


class MoveEffect(Effect):
    """
    Effect to change current location
    """
    def __init__(self, engine: Engine, next_room_id: int):
        """
        :param engine: main object in the game
        :param next_room_id: room id room to go to
        """
        self.engine = engine
        self.next_room_id = next_room_id

    def exec(self):
        """Changes current location according to next_room_id"""
        self.engine.go_to_room(self.next_room_id)
