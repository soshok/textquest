from effects.effect import Effect


class Room:
    def __init__(self, room_id: int, name: str, description: str, actions: 'dict[str, list[Effect]]',
                 is_exit_room: bool):
        """
        :int room_id: unique room id
        :str name: current location name
        :str description: description of the current location
        :dict[str, list[Effect]] actions: available actions in current room. key: action name, value: associated effects
        :bool is_exit_room: exit room flag (the last room in a game)
        """
        self.id = room_id
        self.name = name
        self.description = description
        self.actions = actions
        self.is_exit_room = is_exit_room

    def __str__(self):
        return f"room_id: {self.id}\nname: {self.name}\ndescription: {self.description}\nactions: {self.actions}"
