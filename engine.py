from room_interface.room_interface import RoomInterface
from room import Room


class Engine:
    """
    This is the main class in the game. It links the game modules
    """
    def __init__(self):
        self.room_reader = RoomInterface()
        self.current_room = Room(-1, '', '', {}, True)

    def set_room_interface(self, room_reader: RoomInterface):
        self.room_reader = room_reader

    def go_to_room(self, room_id: int):
        """
        Parse a new room and sets one to current_room
        """
        self.current_room = self.room_reader.get_room_by_id(room_id)

    def take_action(self, action_name: str):
        """
        take an action selected by name
        :raise RuntimeError if action is not available
        """
        actions = self.current_room.actions

        if action_name not in actions.keys():
            raise RuntimeError(f'Невозможно выполнить данное действие: "{action_name}"')

        for act in actions[action_name]:
            act.exec()

    def available_actions(self):
        """
        :return: available actions in the current_room
        """
        return self.current_room.actions.keys()
